package org.fundacaonokia.q13;

public class Node {
	public int value;
	public Node next;
	public Node prev;
	
	public Node(int n){
		value = n;
		next = null;
	}
	
	public Node(int n, Node pNext) {
		value = n;
		next = pNext;
	}
	
	public Node()
	{ }
}
