package org.fundacaonokia.q13;

public class LinkedList implements List {
	protected Node begin, end;
	protected int cnt = 0;
	
	public LinkedList() 
	{ }
	
	public Node getBegin() {
		return begin;
	}
	
	@Override
	public void add(int n) {
		// TODO Auto-generated method stub
		if(cnt != 0) {
			Node aux = new Node(n);
			end.next = aux;
			end = aux;
		}
		else {
			begin = new Node(n);
			end = begin;
		}
		
		cnt++;
	}

	@Override 
	public void add(int index, int n) {
		// TODO Auto-generated method stub
		if(index < cnt) {
			Node search = begin;
			
			for(int i = 0; i < index; i++) {
				search = search.next;
			}
			
			Node aux = new Node(n, search.next);
			search.next = aux;
			end = aux;
		}
	}

	@Override
	public void addAll(int[] vet) {
		// TODO Auto-generated method stub
		for (int i = 0; i < vet.length; i++) {
			this.add(vet[i]);
		}
	}

	@Override
	public void addAll(List lst) {
		// TODO Auto-generated method stub
		this.addAll(lst.toArray());
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		begin = null;
		end = null;
		cnt = 0;
	}

	@Override
	public boolean contains(int n) {
		// TODO Auto-generated method stub
		Node search = begin;
		
		while(search != null) {
			if(search.value == n) return true;
			else search = search.next;
		}
		
		return false;
	}

	@Override
	public boolean containsAll(List lst) {
		// TODO Auto-generated method stub
		Node search = begin;
		if(!lst.isEmpty()) {
			for(int i = 0; search != null; i++) {
				if(search.value != lst.get(i)) return false;
				else search = search.next;
			}
		}
		
		return true;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (cnt <= 0);
	}

	@Override
	public int indexOf(int n) {
		// TODO Auto-generated method stub
		Node search = begin;
		
		for(int i = 0; search != null; i++) {
			if(search.value == n) return i;
			else search = search.next;
		}
		
		return -1;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return cnt;
	}

	@Override
	public int get(int pos) {
		// TODO Auto-generated method stub
		Node search = begin;
		
		for(int i = 0; i < pos; i++) {
			search = search.next;
		}
		
		return search.value;
	}

	@Override
	public int remove() {
		// TODO Auto-generated method stub
		int toReturn = begin.value;
		begin = begin.next;
		cnt--;
		
		return toReturn;
	}

	@Override
	public int remove(int pos) {
		// TODO Auto-generated method stub
		int toReturn = -1;
		
		if(!(pos >= cnt)) {
			Node search = this.begin;
			
			if(pos == 0) {
				toReturn = begin.value;
				begin = begin.next;
			}
			else {
				for(int i = 0; i < pos; i++) {
					search = search.next;
				}
			
				if(pos < cnt - 1) {
					toReturn = search.next.value;
					search.next = search.next.next;
				}
				else if(pos == cnt - 1) {
					this.end = search;
				}
			}
		}
		
		cnt--;
		
		return toReturn;
	}

	@Override
	public int[] toArray() {
		// TODO Auto-generated method stub
		int[] array = new int[cnt];
		Node search = this.begin;
		
		for (int i = 0; search != null; i++) {
			array[i] = search.value;
			search = search.next;
		}
		
		return array;
	}
	
}
