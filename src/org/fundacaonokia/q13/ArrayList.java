package org.fundacaonokia.q13;


public class ArrayList implements List{
	
	private int[] numbers;
	private int length = 10;
	private int cnt = 0;
	
	public ArrayList() {
		numbers = new int[length];
	}
	
	private void realoc() {
		int[] auxArray = numbers;
		length *= 2;
		numbers = new int[length];
		
		for (int i = 0; i < auxArray.length; i++) {
			numbers[i] = auxArray[i];
		}
	}
	
	private void fullCheck() {
		if(cnt >= length) {
			realoc();
		}
	}
	
	@Override
	public void add(int n) {
		fullCheck();
		
		numbers[cnt] = n;

		cnt++;
	}
	
	@Override
	public void add(int index, int n) {
		// TODO Auto-generated method stub
		fullCheck();
		
		for (int i = cnt + 1; i > index; i--) {
			numbers[i] = numbers[i - 1]; 
		}
		
		numbers[index] = n;
		
		cnt++;
	}

	@Override
	public void addAll(int[] vet) {
		// TODO Auto-generated method stub
		for (int i = 0; i < vet.length; i++) {
			this.add(vet[i]);
		}
	}

	@Override
	public void addAll(List lst) {
		// TODO Auto-generated method stub
		int lstSize = lst.size();
		for (int i = 0; i < lstSize; i++) {
			this.add(lst.get(i));
		}
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		cnt = 0;
		this.numbers = new int[10];
	}

	@Override
	public boolean contains(int n) {
		for (int i = 0; i < cnt; i++) {
			if(this.numbers[i] == n) return true;
		}
		return false;
	}

	@Override
	public boolean containsAll(List lst) {
		if(!lst.isEmpty()) {
			for (int i = 0; i < cnt; i++) {
				if(this.numbers[i] != lst.get(i)) return false;
			}
		}
		
		return true; //todo conjunto contem o vazio, logo, se a lista estiver vazia, o m�todo retornar� true :P
	}

	@Override
	public boolean isEmpty() {
		return (cnt <= 0);
	}

	@Override
	public int indexOf(int n) {
		for (int i = 0; i < cnt; i++) {
			if(this.numbers[i] == n) return i;
		}
		return -1;
	}

	@Override
	public int size() {
		return cnt;
	}

	@Override
	public int get(int pos) {
		return numbers[pos];
	}

	@Override
	public int remove() {
		cnt--;
		int toReturn = numbers[cnt];
		numbers[cnt] = 0;
		return toReturn;
	}

	@Override
	public int remove(int pos) {
		if(pos < cnt) {
			int toReturn = this.numbers[pos];
			
			for (int i = pos; i < cnt - 1; i++) {
				numbers[i] = numbers[i + 1];
			}
			
			cnt--;
			return toReturn;
		}
		
		return -1;
	}

	@Override
	public int[] toArray() {
		return this.numbers;
	}
	
}
