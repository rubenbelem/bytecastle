package org.fundacaonokia.q13;

public class Stack extends LinkedList {
	public Stack() {
		this.begin = this.end = null;
	}
	
	@Override
	public void add(int n) {
		Node aux = end;
		super.add(n);
		if(end != null) {
			end.prev = aux;
		}
	}
	
	@Override
	public int remove() {
		int toReturn = end.value;
		end = end.prev;
		cnt--;
		
		return toReturn;
	}
}
