package org.fundacaonokia.q14;

public class SortedArrayList implements List {

	private int[] numbers;
	private int length = 10;
	private int cnt = 0;

	public SortedArrayList() {
		numbers = new int[length];
	}

	public void reOrder() {
		for (int i = 0; i < this.size() - 1; i++) {
			for (int j = i + 1; j < this.size() - 2; j++) {
				if (numbers[i] > numbers[j]) {
					int aux = numbers[i];
					numbers[i] = numbers[j];
					numbers[j] = aux;
				}
			}
		}
	}

	private void realoc() {
		int[] auxArray = numbers;
		length *= 2;
		numbers = new int[length];

		for (int i = 0; i < auxArray.length; i++) {
			numbers[i] = auxArray[i];
		}
	}

	private void fullCheck() {
		if (cnt >= length) {
			realoc();
		}
	}

	public void pushToFront() {
		fullCheck();

		int aux[] = new int[numbers.length + 1];

		aux[0] = 0;
		for (int i = 0; i < size(); i++) {
			aux[i + 1] = numbers[i];
		}

		for (int i = 0; i < size() + 1; i++) {
			numbers[i] = aux[i];
		}

	}

	@Override
	public void add(int n) {
		fullCheck();

		if (cnt > 0) {
			pushToFront();
		}

		numbers[0] = n;

		cnt++;

		reOrder();
	}

	@Override
	public void add(int index, int n) {
		// TODO Auto-generated method stub
		fullCheck();

		for (int i = cnt + 1; i > index; i--) {
			numbers[i] = numbers[i - 1];
		}

		numbers[index] = n;

		cnt++;

		reOrder();
	}

	@Override
	public void addAll(int[] vet) {
		// TODO Auto-generated method stub
		for (int i = 0; i < vet.length; i++) {
			this.add(vet[i]);
		}
		reOrder();
	}

	@Override
	public void addAll(List lst) {
		// TODO Auto-generated method stub
		int lstSize = lst.size();
		for (int i = 0; i < lstSize; i++) {
			this.add(lst.get(i));
		}
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		cnt = 0;
		this.numbers = new int[10];
	}

	@Override
	public boolean contains(int n) {
		for (int i = 0; i < cnt; i++) {
			if (this.numbers[i] == n)
				return true;
		}
		return false;
	}

	@Override
	public boolean containsAll(List lst) {
		if (!lst.isEmpty()) {
			for (int i = 0; i < cnt; i++) {
				if (this.numbers[i] != lst.get(i))
					return false;
			}
		}

		return true; // todo conjunto contem o vazio, logo, se a lista estiver
						// vazia, o m�todo retornar� true :P
	}

	@Override
	public boolean isEmpty() {
		return (cnt <= 0);
	}

	@Override
	public int indexOf(int n) {
		for (int i = 0; i < cnt; i++) {
			if (this.numbers[i] == n)
				return i;
		}
		return -1;
	}

	@Override
	public int size() {
		return cnt;
	}

	@Override
	public int get(int pos) {
		return numbers[pos];
	}

	public void afterRemove() {
		
		for (int i = 0; i < size(); i++) {
			numbers[i] = numbers[i + 1];
		}
	}

	@Override
	public int remove() {
		cnt--;
		int toReturn = numbers[0];
		numbers[0] = 0;
		afterRemove();
		return toReturn;
	}

	public String toString() {
		String toreturn = "";
		for (int i = 0; i < size(); i++) {
			toreturn += " " + numbers[i];
		}
		return toreturn;
	}

	@Override
	public int remove(int pos) {
		if (pos < cnt) {
			int toReturn = this.numbers[pos];

			for (int i = pos; i < cnt - 1; i++) {
				numbers[i] = numbers[i + 1];
			}

			cnt--;
			return toReturn;
		}

		return -1;
	}

	@Override
	public int[] toArray() {
		return this.numbers;
	}

	public static void main(String[] args) {
		SortedArrayList Rubem = new SortedArrayList();

		Rubem.add(2);
		Rubem.add(1);
		Rubem.add(1);
		Rubem.add(1);
		Rubem.add(-5);
		Rubem.remove();

		System.out.println(Rubem.toString());

	}

}
