package org.fundacaonokia.q14;

public interface Comparable {
	int compareTo(Comparable c);
}
