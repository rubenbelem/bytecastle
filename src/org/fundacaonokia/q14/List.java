package org.fundacaonokia.q14;

public interface List {
	void add(int n);
	void add(int index, int n);
	void addAll(int vet[]);
	void addAll( List lst );
	void clear();
	
	boolean contains( int n );
	boolean containsAll( List lst );
	boolean isEmpty();
	
	int indexOf( int n );
	int size();
	int get(int pos);
	int remove();
	int remove(int pos);
	int[] toArray();
}
