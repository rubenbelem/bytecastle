package org.fundacaonokia.q14;

public class Node implements Comparable {
	public int value;
	public Node next;
	public Node prev;
	
	public Node(int n){
		value = n;
		next = null;
	}
	
	public Node(int n, Node pNext) {
		value = n;
		next = pNext;
	}
	
	public Node()
	{ }

	@Override
	public int compareTo(Comparable c) {
		if (c != null) {
			if (this.getClass() == c.getClass()) {
				Node aux = (Node) c;
				
				if (this.value == aux.value) {
					return 0;	
				}
				else if(this.value > aux.value) {
					return 1;
				}
				else return -1;
			}
		}
		
		return -1;
	}
}
