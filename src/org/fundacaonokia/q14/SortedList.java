package org.fundacaonokia.q14;

public class SortedList extends LinkedList {
	
	public SortedList() {
		this.cnt = 0;
	}
	
	@Override
	public void add(Comparable n) {
		if(this.cnt > 0) {
			if(n.compareTo(this.begin.value) <= 0) {
				Node aux = new Node(n, this.begin);
				this.begin = aux;
			}
			else {
				Node search = this.begin;
				Node prev = search;
				
				while(search != null) {
					int equals = search.compareTo(n)
					if(search.value >= n) break;
					prev = search;
					search = search.next;
				}
				
				Node aux = new Node(n, prev.next);
				prev.next = aux;
			}
		}
		else {
			this.begin = new Node(n);
			this.end = this.begin;
		}
		
		this.cnt++;
	}
}
